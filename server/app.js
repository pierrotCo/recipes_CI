const express=require("express");
const bodyParser=require("body-parser");
const cors=require("cors");
const http=require("http");
const socket=require("socket.io");
const routes = require('./routes/routes');
const mongoose=require("mongoose");


mongoose.connect("mongodb://localhost/DTY_recipes");

var app=express();
app.use(cors({origin:"http://localhost:4200"}));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(routes);

var server = http.Server(app);
var io = socket(server); 

io.on('connection', (socket) => {
    socket.on('nouveau-message', (obj) => {
        io.emit('nouveau-message', obj);
  });
});

server.listen(8080);


