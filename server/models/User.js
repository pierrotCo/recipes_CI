const mongoose=require("mongoose");
const crypto = require('crypto');
const jwt = require('jsonwebtoken');

var SchemaUser=new mongoose.Schema({
    account : {type:String, required:true},
    hash: { type:String},
    salt: {type: String},
    recettesTestees: {type:Array, required:true}
});

SchemaUser.methods.setPassword = function(password) {
    if(password.length>=4){
        this.salt = crypto.randomBytes(16).toString('hex');
        this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
    } 
  };

  SchemaUser.methods.validatePassword = function(password) {
    const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
    return this.hash === hash;
  };
  
  SchemaUser.methods.generateJWT = function() {
    const today = new Date();
    const expirationDate = new Date(today);
    expirationDate.setDate(today.getDate() + 60);
  
    return jwt.sign({
      account: this.account,
      recettesTestees: this.recettesTestees,
      exp: parseInt(expirationDate.getTime() / 1000, 10),
    }, 'secret');
  }
  
  SchemaUser.methods.toAuthJSON = function() {
    return {
      account: this.account,
      recettesTestees: this.recettesTestees,
      token: this.generateJWT(),
    };
  };

mongoose.model("User", SchemaUser);