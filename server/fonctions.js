module.exports.clean = function(ingredients){
    ingredientsClean=[];
    for(let ingredient of ingredients){
        if(ingredient.nom!="" && ingredient.quantite!=0){
            ingredientsClean.push(ingredient);
        }
    }
    return ingredientsClean;
}

module.exports.genereLien = function(nom){
    let s="";
    for(let lettre of nom.toLowerCase()){
        s+=lettre;
        if(lettre=="é" || lettre=="è" || lettre=="ê"){
            s=s.slice(0,-1);
            s+="e";
        }
        if(lettre=="à" || lettre=="â"){
            s=s.slice(0,-1);
            s+="a";
        }
        if(lettre=="ô"){
            s=s.slice(0,-1);
            s+="o";
        }
        if(lettre=="ù"){
            s=s.slice(0,-1);
            s+="o";
        }
        if(lettre=="î"){
            s=s.slice(0,-1);
            s+="i";
        }
        if(lettre==" "){
            s=s.slice(0,-1);
            s+="-";
        }
    }
    return s;
}

function ressemblance(recette1,recette2){
    console.log(recette1);
    score=0;
    for(let ingredient1 of recette1.ingredients){
        for(let ingredient2 of recette2.ingredients){
            if(genereLien(ingredient1.nom)==genereLien(ingredient2.nom.toLowerCase())){
                score+=1;
            }
        }
    }
    return score;
}

function suggere(recette1,recettes){
    scoreMax=0;
    recetteSuggeree=recettes[0];
    score=0;
    for(let recette2 of recettes){
        score=ressemblance(recette1,recette2);
        if(score>scoreMax && recette2.nom!=recette1.nom){
            score=scoreMax;
            recetteSuggeree=recette2;
        }
    }
    return recetteSuggeree;
}

function suggereListe(recettesTestees){
    l=[];
    for(let recette of recettesTestees){
        recetteSuggeree=suggere(recette,recettes);
        if(!recettesTestees.includes(recetteSuggeree) && l.includes(recetteSuggeree)){
            l.push(recetteSuggeree);
        }
    }
    return l;
}

